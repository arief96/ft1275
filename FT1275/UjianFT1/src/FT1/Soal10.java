package FT1;

import java.util.Scanner;

public class Soal10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("input n : ");
		String abjad = sc.nextLine();
		String t = abjad.replace(" ", "").toLowerCase();
		String[]aAbjad=t.split("");
		char[]aChar=new char[aAbjad.length];
		
		//fill
		for (int i = 0; i < aChar.length; i++) {
			aChar[i]=aAbjad[i].charAt(0);
		}
		
//		//ngecek
//		for (char s : aChar) {
//			System.out.print(s+" ");
//		}
		//sort atau mengurutkan
		char wadah;
		for (int i = 0; i < aChar.length; i++) {
			for (int j = 0; j < aChar.length-1; j++) {
				if(aChar[j]>aChar[j+1]) {
					wadah=aChar[j];
					aChar[j]=aChar[j+1];
					aChar[j+1]=wadah;
				}
				
			}
		}
		//
		System.out.println();
		String tampungVokal="";
		String tampungKonson="";
		for (int i = 0; i < aChar.length; i++) {
			switch (aChar[i]) {
			case 'a':
				tampungVokal = tampungVokal + String.valueOf(aChar[i]);
				break;
			case 'i':
				tampungVokal = tampungVokal + String.valueOf(aChar[i]);
				break;
			case 'u':
				tampungVokal = tampungVokal + String.valueOf(aChar[i]);
				break;
			case 'e':
				tampungVokal = tampungVokal + String.valueOf(aChar[i]);
				break;
			case '0':
				tampungVokal = tampungVokal + String.valueOf(aChar[i]);
				break;
			default:
				tampungKonson = tampungKonson + String.valueOf(aChar[i]);
				break;
			}
		}
		System.out.println("Huruf vokal "+tampungVokal);
		System.out.println("Huruf Konsonan "+tampungKonson);
		
		String arrT1[] = tampungVokal.split("");
		String aFilter[] = new String[arrT1.length];
		int posisi=0;
		for (int i = 0; i < arrT1.length; i++) {
			boolean kembar = false;
			for (int j = 0; j < aFilter.length; j++) {
				if (arrT1[i].equals(aFilter[j])) {
					kembar = true;
					break;
				}
			}
			if (kembar == false) {
				aFilter[posisi] = arrT1[i];
				posisi++;
			}

		}
		for (String string : aFilter) {
			System.out.print(string+" ");
		}
		System.out.println();
		
		String arrK[] = tampungKonson.split("");
		String aFilter2[] = new String[arrK.length];
		int posisi2=0;
		for (int i = 0; i < arrK.length; i++) {
			boolean kembar = false;
			for (int j = 0; j < aFilter2.length; j++) {
				if (arrK[i].equals(aFilter2[j])) {
					kembar = true;
					break;
				}
			}
			if (kembar == false) {
				aFilter2[posisi2] = arrK[i];
				posisi2++;
			}

		}
		for (String string : aFilter2) {
			System.out.print(string+" ");
		}
		//second
//		System.out.println();
//		for (char c : aChar) {
//			System.out.print(c+" ");
//		}
		sc.close();
	}

}
