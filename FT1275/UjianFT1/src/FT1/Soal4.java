package FT1;

import java.util.Scanner;

public class Soal4 {
	//Dengan hanya menggunakan logic, ubah format jam dari 24H ke 12H dan juga sebaliknya
	//
	//contoh:
	//input: 12:35 AM
	//output: 00:35
	//
	//input: 19:30
	//output: 07:30 PM
	//
	//input: 09:05
	//output: 09:05 AM
	//
	//input: 11:30 PM
	//output: 23:30
	//
	//NB: perbedaan format 12H dan 24H ada pada jam 00 tengah malam (jam 00 adalah jam 12 AM, lihat contoh 1), selebihnya tinggal menambahkan PM antara jam 12:00 - 23:59 dan AM antara jam 00:00 - 11:59
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		//String aMpM = "04:43:AM";
		System.out.print("input Jam : ");
		String jam = sc.nextLine();
		//String jam = "04:43AM";
		//string jam = 04:30;
		if (jam.length()==5) {
			int hh = Integer.parseInt(jam.substring(0, 2));//ambil jam dari input
			if (hh==0) {
				hh = hh + 12;
				System.out.print(hh);//cetak jam
				System.out.print(jam.substring(2, 5)+" AM");//cetak menit
			} else if (hh>12) {
				hh = hh - 12;
				System.out.print(hh);//cetak jam
				System.out.print(jam.substring(2, 5)+" PM");//cetak menit	
			} else if (hh==12) {
				System.out.print(hh);//cetak jam
				System.out.print(jam.substring(2, 5)+" PM");//cetak menit
			} else {
				System.out.print(hh);//cetak jam
				System.out.print(jam.substring(2, 5)+" AM");//cetak menit
			}
		} else {
			int hh = Integer.parseInt(jam.substring(0, 2));//ambil jam dari input
			if (jam.charAt(5)=='A') {
				//am
				if (hh==12) {
					hh = hh - 12;
					System.out.print(hh);//cetak jam
					System.out.print(jam.substring(2, 5));//cetak menit
				} else {
					System.out.print(hh);//cetak jam
					System.out.print(jam.substring(2, 5));//cetak menit
				}
			} else {
				if (hh==12) {
					//pm
					System.out.print(hh);//cetak jam
					System.out.print(jam.substring(2, 5));//cetak menit
				} else {
					hh = hh + 12;
					System.out.print(hh);//cetak jam
					System.out.print(jam.substring(2, 5));//cetak menit
				}
			}
		}
//		if (jam.length()>5) {
//			int hh = Integer.parseInt(jam.substring(0, 2));
//			//
//			if (jam.charAt(5)=='A') {
//				//am
//				if (hh==12) {
//					//cetak jam
//					System.out.print("00");
//					//cetak mm
//					System.out.println(jam.substring(2,5));
//				} else {
//					System.out.println(jam.substring(0,5));
//				}
//			} else {
//				//pm
//				if (hh>12) {
//					//jam
//					System.out.print("12");
//					//mm
//					System.out.println(jam.substring(2,5));
//				} else {
//					hh=hh+12;
//					System.out.print(hh);
//					System.out.print(jam.substring(2,5));
//				}
//			}
//		} else {
//			int hh = Integer.parseInt(jam.substring(0, 2));
//			//
//			if (hh>12) {
//				//cetak jam
//				hh=hh-12;
//				System.out.print(hh);
//				//cetak mm
//				System.out.println(jam.substring(2,5)+" PM");
//			} else {
//				hh=hh+12;
//				System.out.println(jam.substring(0,5)+" AM");
//				//cetak mm
//				//System.out.println(jam.substring(2,5));
//			}
//		}
	}

}
