package FT1;

import java.util.Scanner;

public class Soal1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("masukan angka : ");
		int n = sc.nextInt();
		int[] aTiga = new int[n];
		int[] aEmpat = new int[n];
		int[] arrGenap = new int[n];
		int[] arrGanjil = new int[n];
		//kelipatan 3
		int x=0, y=1;
		for (int i = 0; i < n; i++) {
			x = (3*y)-1; // x = (x*3)-1
			aTiga[i] = x;
			y++;
		}
		System.out.print("deret kel 3 kurang 1 : ");
		for (int i : aTiga) {
			System.out.print(i+" ");
		}
		System.out.println();
		//kelipatan 4 dibagi 2
		int k=0, l=1;
		for (int i = 0; i < n; i++) {
			k = (4*l)/2; // x = (x*3)-1
			aEmpat[i] = k;
			l++;
		}
//		
		System.out.print("deret Kel 4 dibagi 2 : ");
		for (int i : aEmpat) {
			System.out.print(i+" ");
		}
//		
		System.out.println();
		System.out.print("Jumlah index genap dan ganjil ");
		for (int i = 0; i < n; i++) {
			if (i%2==0) {
				arrGanjil[i] = aTiga[i]+aEmpat[i];
			} else {
				arrGenap[i] = aTiga[i]+aEmpat[i];
			}
			System.out.print(aTiga[i]+aEmpat[i]+" ");
		}
		System.out.println();
		System.out.print("Deret ganjil ");
		for (int i : arrGanjil) {
			if (i==0) {
				System.out.print("");
			}else {
				System.out.print(i+" ");
			}
			
		}
		System.out.println();
		System.out.print("Deret genap ");
		for (int i : arrGenap) {
			if (i==0) {
				System.out.print("");
			}else {
				System.out.print(i+" ");
			}
			//System.out.print(i+" ");
		}
	}

}
